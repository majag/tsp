# modul s funkcijama genetskog algoritma
# kod genetskog algoritma preuzet s http://pastebin.com/wgLaBbWR
# i preraden za potrebe ovog projekta

import random
import copy


def krizanje(gradoviA, gradoviB):
    """
    Funkcija koja kriza dvije ture da dobijemo novu turu. Koristi 'greedy
    crossover' algoritam.

    Argumenti:
    gradoviA -- lista gradova redom kojim se prolaze u turi; elementi liste su
    oznake gradova
    
    gradoviB -- lista gradova redom kojim se prolaze u turi; elementi liste su
    oznake gradova

    Vraca:
    gradovi_dijete - lista gradova redom kojim ce se proci u novoj turi.
    Dobivena je krizanjem listi 'gradoviA' i 'gradoviB'.
    """

    flagA = True
    flagB = True
    broj_gradova = len(gradoviA)
    t = random.choice(gradoviA)
    x = gradoviA.index(t)
    y = gradoviB.index(t)
    gradovi_dijete = [t]

    while flagA == True or flagB == True:
        x = (x-1) % broj_gradova
        y = (y+1) % broj_gradova

        if flagA == True:
            if gradoviA[x] not in gradovi_dijete:
                gradovi_dijete.insert(0, gradoviA[x])
            else:
                flagA = False
        if flagB == True:
            if gradoviB[y] not in gradovi_dijete:
                gradovi_dijete.append(gradoviB[y])
            else:
                flagB = False

    if len(gradovi_dijete) < broj_gradova:
        ostatak_gradova = []
        for grad in gradoviA:
            if grad not in gradovi_dijete:
                ostatak_gradova.append(grad)
        random.shuffle(ostatak_gradova)
        gradovi_dijete += ostatak_gradova

    return gradovi_dijete


def mutacija(tura, postotak):
    """
    Funkcija koja ovisno o postotku mutira ili ne mutira turu, odnosno mijenja 
    redoslijed obidenih gradova.

    Argumenti:
    tura -- lista gradova redom kojim su posjeceni u trazenju najboljeg
    rjesenja

    postotak -- broj koji nam odreduje u koliko posto slucajeva ce se dogoditi
    mutacija ture

    Vraca:
    a) Ako je slucajno generirani broj manji od zadanog postotka:
    mutirana_tura - lista koja sadrzi izmjenjeni popis gradova koji
    su posjeceni

    b) Ako je slucajno generirani broj veci od zadanog postotka:
    tura - nepromijenjena lista gradova redom kojim su posjeceni u trazenju
    najboljeg rjesenja; dobivena kao argument
    """

    if random.random() < postotak:
        i = j = 0
        mutirana_tura = copy.copy(tura)
        while i == j:
            i = random.randint(0, len(tura)-1)
            j = random.randint(0, len(tura)-1)
        if i > j:
            i, j = j, i
        while i < j:
            mutirana_tura[i], mutirana_tura[j] = mutirana_tura[j], mutirana_tura[i]
            i += 1
            j -= 1
        return mutirana_tura
    else:
        return tura


def selekcija(gradovi_djeca):
    """
    Izabire turu iz liste turi na temelju 'roulette wheel selection' algoritma;
    svakoj turi će se dodijeliti 'fitness' broj koji utjece na vjerojatnost
    odabira te ture. 

    Argumenti:
    gradovi_djeca -- lista koja sadrzi popis turi
    
    Vraca:
    gradovi_djeca[rank] - lista (tura) gradova koja je izabrana iz popisa svih
    turi prema 'roulette wheel selection' algoritmu
    """

    gradovi_djeca.sort()
    polje = []
    duljina = len(gradovi_djeca)
    ukupno = 0.0
    for i in range(1, duljina+1):
        ukupno += i
    for i in range(duljina, 0, -1):
        polje.append(i/ukupno)
    
    rand_broj = random.random()
    for rank, postotak in enumerate(polje):
        if rand_broj < postotak:
            return gradovi_djeca[rank]
        else:
            rand_broj -= postotak
