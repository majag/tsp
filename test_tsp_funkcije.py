# modul s test funkcijama za f-je modula "tsp_funkcije.py"

import tsp_funkcije as tsp_f
from math import sqrt


def test_cijena_ture():
	matrica = {}
	matrica[(0,0)] = matrica[(1,1)] = matrica[(2,2)] = 0
	matrica[(0,1)] = matrica[(1,0)] = 4
	matrica[(0,2)] = matrica[(2,0)] = 5
	matrica[(1,2)] = matrica[(2,1)] = 3
	tura = [2, 1, 0, 2]

	cijena = tsp_f.cijena_ture(tura, matrica)
	assert cijena == 12

	print("Test za f-ju 'cijena_ture()' uspjesno prolazi.")


def test_citaj_koordinate():
	popis_gradova = "test_gradovi_brojevi.txt"
	ocekivani_gradovi = [0, 1, 2]
	ocekivane_koordinate = [(123, 98), (421, 245), (231, 56)]
	koordinate, gradovi = tsp_f.citaj_koordinate(popis_gradova)

	assert len(gradovi) == 3
	assert len(koordinate) == 3
	assert ocekivane_koordinate == koordinate
	assert ocekivani_gradovi == gradovi

	print("Test za f-ju 'citaj_koordinate()' uspjesno prolazi.")


def test_matrica_udaljenosti():
	koordinate = [(0,0), (0,1), (1,0), (1,1)]
	gradovi = [0, 1, 2, 3]
	matrica = tsp_f.matrica_udaljenosti(koordinate, gradovi)

	assert len(matrica) == 16
	assert matrica[(0,0)] == 0
	assert matrica[(1,1)] == 0
	assert matrica[(2,2)] == 0
	assert matrica[(3,3)] == 0
	assert matrica[(0,1)] == 1
	assert matrica[(1,0)] == 1
	assert matrica[(0,2)] == 1
	assert matrica[(2,0)] == 1
	assert matrica[(0,3)] == sqrt(2)
	assert matrica[(3,0)] == sqrt(2)
	assert matrica[(1,2)] == sqrt(2)
	assert matrica[(2,1)] == sqrt(2)
	assert matrica[(1,3)] == 1
	assert matrica[(3,1)] == 1
	assert matrica[(2,3)] == 1
	assert matrica[(3,2)] == 1

	print("Test za f-ju 'matrica_udaljenosti()' uspjesno prolazi.")


def test_inic_posjeceni():
	gradovi = [0, 1, 2]
	posjeceni = tsp_f.inic_posjeceni(gradovi)

	assert len(posjeceni) == 3
	assert posjeceni[0] == 0
	assert posjeceni[1] == 0
	assert posjeceni[2] == 0

	print("Test za f-ju 'inic_posjeceni()' uspjesno prolazi.")


def test_inic_tura():
	gradovi = [2, 1, 0]
	posjeceni = [0, 0, 0]
	tura, posjeceni_novi = tsp_f.inic_tura(posjeceni, gradovi)
	
	assert len(tura) == 1
	assert len(posjeceni_novi) == 3
	assert tura[0] == 2
	assert posjeceni_novi[0] == 1
	assert posjeceni_novi[1] == 0
	assert posjeceni_novi[2] == 0

	print("Test za f-ju 'inic_tura()' uspjesno prolazi.")


def test_tsp_algoritam():
	koordinate = [(0,0), (4,0), (4,3)]
	gradovi = [0, 1, 2]
	posjeceni = [1, 0, 0]
	tura = [0]

	matrica = tsp_f.matrica_udaljenosti(koordinate, gradovi)
	assert len(matrica) == 9

	posjeceni, tura, ukupna_cijena = tsp_f.tsp_algoritam(matrica, posjeceni, tura, gradovi)
	
	assert len(posjeceni) == 3
	assert len(tura) == 4
	assert posjeceni[0] == 1
	assert posjeceni[1] == 1
	assert posjeceni[2] == 1
	assert tura[0] == 0
	assert tura[1] == 1
	assert tura[2] == 2
	assert tura[3] == 0
	assert ukupna_cijena == 12

	print("Test za f-ju 'tsp_algoritam()' uspjesno prolazi.")


def test_tsp_algoritam_GA():
	koordinate = [(0,0), (4,0), (4,3)]
	gradovi = [0, 1, 2]
	posjeceni = [1, 0, 0]
	tura = [0]

	matrica = tsp_f.matrica_udaljenosti(koordinate, gradovi)
	assert len(matrica) == 9

	posjeceni, tura, ukupna_cijena = tsp_f.tsp_algoritam_GA(matrica, posjeceni, tura, gradovi)
	
	assert len(posjeceni) == 3
	assert len(tura) == 3
	assert posjeceni[0] == 1
	assert posjeceni[1] == 1
	assert posjeceni[2] == 1
	assert tura[0] == 0
	assert tura[1] == 1
	assert tura[2] == 2
	assert ukupna_cijena == 7

	print("Test za f-ju 'tsp_algoritam_GA()' uspjesno prolazi.")


def main():
	test_cijena_ture()
	test_citaj_koordinate()
	test_matrica_udaljenosti()
	test_inic_posjeceni()
	test_inic_tura()
	test_tsp_algoritam()
	test_tsp_algoritam_GA()


if __name__ == "__main__":
	main()
