# modul s funkcijama osnovnog algoritma TSP-a
# + funkcija koja racuna ukupnu cijenu puta za danu turu

from math import sqrt
from operator import itemgetter


def cijena_ture(tura, matrica):
    """
    Racuna ukupnu cijenu ture za danu turu gradova kojima se prolazi.

    Argumenti:
    tura -- lista gradova kojima se prolazi u turi, elementi su oznake gradova

    matrica -- dictionary sa udaljenostima izmedu svaka dva postojeca grada

    Vraca:
    Broj tipa float - broj ciji je iznos jednak ukupnoj cijeni puta za danu
    turu prolaska gradovima.
    """

    ukupna_cijena = 0
    for i in range(len(tura)-1):
        gradA = tura[i]
        gradB = tura[i+1]
        ukupna_cijena += matrica[gradB,gradA]
    return ukupna_cijena


def citaj_koordinate(popis_gradova):
    """
    Otvara datoteku s popisom gradova i njihovim x i y koordinatama te je cita,
    a brojeve gradova i njihove koordinate zapisuje u dvije odvojene liste.
    Datoteka mora biti izgleda:
    grad1 x1 y1
    grad2 x2 y2
    ...
    gradN xN yN, gdje su gradovi i koordinate odvojeni 'tabom'.

    Argumenti:
    popis_gradova -- naziv datoteke iz koje zelimo citati

    Vraca:
    Dvije liste.
    
    Prva lista(koordinate) - koordinate gradova redom kojim su citani, svaki
    element liste je tuple oblika (x, y).
    
    Druga lista(gradovi) - lista gradova redom kojim su citani, elementi su
    oznake gradova.
    """

    datoteka = open(popis_gradova)
    koordinate = []
    gradovi = []
    for linija in datoteka:
        grad, x, y = linija.split("\t")
        koordinate.append((float(x), float(y)))
        gradovi.append(int(grad))
    datoteka.close()
    return koordinate, gradovi


def matrica_udaljenosti(koordinate, gradovi):
    """
    Iz dane liste koordinata funkcija radi matricu udaljenosti izmedu svaka
    dva grada.

    Argumenti:
    koordinate -- lista tuple-ova oblika (x,y) gdje su x i y koordinate grada

    gradovi -- lista gradova redom procitanih iz datoteke, elementi su
    oznake gradova

    Vraca:
    Python dictionary sa udaljenostima izmedu svaka dva grada, velicine n^2,
    gdje je n broj procitanih gradova.
    """

    matrica = {}
    for i,(x1,y1) in enumerate(koordinate):
        for j,(x2,y2) in enumerate(koordinate):
            dx = x2-x1
            dy = y2-y1
            udaljenost = sqrt(dx*dx + dy*dy)
            gradA = gradovi[i]
            gradB = gradovi[j]
            matrica[gradA, gradB] = udaljenost
    return matrica


def inic_posjeceni(gradovi):
    """
    Inicijalizira listu koja sluzi za oznacavanje posjecenih gradova, tj. da
    se vidi je li neki grad posjecen u turi ili ne.

    Argumenti:
    gradovi -- lista sa oznakama svih gradova koji su procitani iz datoteke

    Vraca:
    Listu ciji je svaki element = 0, duljina liste je jednaka broju procitanih
    gradova.
    """

    posjeceni = []
    broj_gradova = len(gradovi)
    for i in range(broj_gradova):
        posjeceni.append(0)
    return posjeceni


def inic_tura(posjeceni, gradovi):
    """
    Inicijalizira listu puta kojim se posjete gradovi; u nju stavlja prvi
    posjeceni grad koji je jednak prvom procitanom iz datoteke. Takoder taj
    isti grad u listi posjecenih oznacava kao posjecenog (umjesto 0 stavlja 1).

    Argumenti:
    posjeceni -- lista ciji je svaki element = 0, duljine iste kao broj
    postojecih gradova
    
    gradovi -- lista procitanih gradova iz datoteke

    Vraca:
    Dvije liste.
    
    Prva lista(tura) - lista u koju je upisan prvi grad; grad iz kojeg se
    krece u obilazak (turu).
    
    Druga lista(posjeceni) - izmjenjena lista "posjeceni"; na poziciji grada
    iz kojeg se krece je umjesto 0 upisano 1.
    """

    prvi_grad = gradovi[0]
    tura = []
    tura.append(prvi_grad)
    posjeceni[0] = 1
    return tura, posjeceni


def tsp_algoritam(matrica, posjeceni, tura, gradovi):
    """
    Funkcija koja daje rjesenje TSP-a pomocu "greedy algoritma".
    Nalazi put kojom se obidu svi gradovi tocno jednom, i vrati u pocetni grad
    te daje cijenu tog puta.

    Argumenti:
    matrica -- dictionary sa udaljenostima izmedu svaka dva postojeca grada

    posjeceni -- lista nula i jedinica koje govore je li neki grad
    postojecen(1) ili nije(0)
    
    tura -- lista s gradom iz kojeg zapocinjemo obilazak svih gradova (turu)
    
    gradovi -- lista svih gradova procitanih iz datoteke

    Vraca:
    Tri podatka: dvije liste i broj tipa float.
    
    Prva lista(posjeceni) - izmjenjena lista dobivena kao argument; lista
    koja sada kao elemente sadrzi sve jedinice(1) jer su na kraju ture svi
    gradovi posjeceni (1 je oznaka posjecenosti).

    Druga lista(tura) - izmjenjena lista dobivena kao argument; lista s popisom
    gradova onim redom kojim su obideni u trazenju rjesenja; zadnji element je
    jednak prvom (moramo se vratiti u grad iz kojeg smo krenuli).
    
    Broj tipa float - broj ciji je iznos jednak cijeni puta kojeg smo
    napravili da obidemo sve gradove; cijena je jednaka udaljenosti potrebnoj
    da se obidu svi gradovi.
    """

    ukupna_cijena = 0
    broj_gradova = len(gradovi)
    
    for i in range(broj_gradova-1):
        zadnji = tura[-1]
        udaljenosti = []
        skok = []
        for j in range(broj_gradova):
            k = gradovi.index(j)
            if zadnji != j and posjeceni[k] == 0:
                udaljenosti.append(matrica[zadnji,j])
                skok.append(j)
        minindex, minvalue = min(enumerate(udaljenosti), key=itemgetter(1))
        ukupna_cijena += minvalue
        tura.append(skok[minindex])
        k = gradovi.index(skok[minindex])
        posjeceni[k] = 1
            
    prvi = tura[0]
    zadnji = tura[-1]
    cijena = matrica[zadnji,prvi]
    ukupna_cijena += cijena
    tura.append(gradovi[0])
    return posjeceni, tura, ukupna_cijena


def tsp_algoritam_GA(matrica, posjeceni, tura, gradovi):
    """
    Funkcija koja daje rjesenje TSP-a pomocu "greedy algoritma".
    Nalazi put kojom se obidu svi gradovi tocno jednom, i NE vrati u pocetni
    grad te daje cijenu tog puta.

    Argumenti:
    matrica -- dictionary sa udaljenostima izmedu svaka dva postojeca grada

    posjeceni -- lista nula i jedinica koje govore je li neki grad
    postojecen(1) ili nije(0)
    
    tura -- lista s gradom iz kojeg zapocinjemo obilazak svih gradova (turu)
    
    gradovi -- lista svih gradova procitanih iz datoteke

    Vraca:
    Tri podatka: dvije liste i broj tipa float.
    
    Prva lista(posjeceni) - izmjenjena lista dobivena kao argument; lista
    koja sada kao elemente sadrzi sve jedinice(1) jer su na kraju ture svi
    gradovi posjeceni (1 je oznaka posjecenosti).

    Druga lista(tura) - izmjenjena lista dobivena kao argument; lista s popisom
    gradova onim redom kojim su obideni u trazenju rjesenja; zadnji element je
    jednak prvom (moramo se vratiti u grad iz kojeg smo krenuli).
    
    Broj tipa float - broj ciji je iznos jednak cijeni puta kojeg smo
    napravili da obidemo sve gradove; cijena je jednaka udaljenosti potrebnoj
    da se obidu svi gradovi.
    """

    ukupna_cijena = 0
    broj_gradova = len(gradovi)
    
    for i in range(broj_gradova-1):
        zadnji = tura[-1]
        udaljenosti = []
        skok = []
        for j in range(broj_gradova):
            k = gradovi.index(j)
            if zadnji != j and posjeceni[k] == 0:
                udaljenosti.append(matrica[zadnji,j])
                skok.append(j)
        minindex, minvalue = min(enumerate(udaljenosti), key=itemgetter(1))
        ukupna_cijena += minvalue
        tura.append(skok[minindex])
        k = gradovi.index(skok[minindex])
        posjeceni[k] = 1

    return posjeceni, tura, ukupna_cijena
