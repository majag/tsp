# glavni program koji poziva sve potrebne funkcije za rijesit problem TSP-a

#! usr/bin/env python3

import tsp_funkcije as tsp_f
import tsp_GA_funkcije as tsp_ga
import random
import copy
#from mpi4py import MPI


# TODO implementirati MPI


broj_generacija = 20000
broj_djece = 15
postotak = 0.3


def main():
    out_file = "results.txt"
    #popis_gradova = "gradovi_brojevi.txt"
    popis_gradova = "gradovi_brojevi1.txt"
    #popis_gradova = "gradovi_brojevi2.txt"

    koordinate, gradovi = tsp_f.citaj_koordinate(popis_gradova)
    matrica = tsp_f.matrica_udaljenosti(koordinate, gradovi)
    posjeceni = tsp_f.inic_posjeceni(gradovi)
    tura, posjeceni = tsp_f.inic_tura(posjeceni, gradovi)

    posjeceni, tura, ukupna_cijena = tsp_f.tsp_algoritam_GA(matrica, posjeceni, tura, gradovi)
    print("Jesu li svi gradovi posjeceni: ", posjeceni)
    print("Tura kojom se posjete gradovi: ", tura)
    print("Ukupna cijena ture: ", ukupna_cijena)


    turaA = copy.copy(tura)
    turaB = copy.copy(tura)
    random.shuffle(turaA)

    trenutna_generacija = 0
    najbolja_cijena = ukupna_cijena

    file = open(out_file, 'w')
    while trenutna_generacija < broj_generacija:
        file.write('\nBroj generacije: ' + str(trenutna_generacija) + '\n')

        ture_djeca = []
        #krizanje i mutacija
        for each in range(broj_djece):
            dijete = tsp_ga.mutacija(tsp_ga.krizanje(turaA, turaB), postotak)
            ture_djeca.append(dijete)
        for i in range(broj_djece):
            file.write('Broj djeteta: ' + str(i) + ', tura: ' + str(ture_djeca[i]) + '\n')
        
        #selekcija
        turaA = tsp_ga.selekcija(ture_djeca)
        nova_cijena = tsp_f.cijena_ture(turaA, matrica)
        
        file.write('Izabrana tura: ' + str(turaA) + '\n')
        file.write('Cijena izabrane ture: ' + str(nova_cijena) + '\n')

        if nova_cijena < najbolja_cijena:
            najbolja_cijena = nova_cijena
            turaB = copy.copy(turaA)
        trenutna_generacija += 1

    print("Tura nakon GA: ", turaB)
    print("Cijena nakon GA: ", najbolja_cijena)

    file.write('\nTura kojom se posjete gradovi: ' + str(tura))
    file.write('\nUkupna cijena ture: ' + str(ukupna_cijena))
    file.write('\nTura nakon GA: ' + str(turaB))
    file.write('\nCijena nakon GA: ' + str(najbolja_cijena))
    file.close()

if __name__ == "__main__":
    main()
