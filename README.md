# Travelling salesman problem 
## project written for the Distributed systems course

The solution was implemented with the help of:

    1. greedy algorithm
    2. genetic algorithm
    3. MPI4Py
    
The solution consists of four main modules:

    1. tsp_glavni_mpi.py (tsp main module)
    2. tsp_funkcije.py (functions module)
    3. tsp_GA_functions.py (functions for genetic algorithm)
    4. test_tsp_funkcije.py (test functions)

    
Input is a txt file with city coordinates (\*gradovi_brojevi\*.txt files)