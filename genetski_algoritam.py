# AI Assignment: TSP with genetic algorithm
# Chang Lan, Last modified: 12-11-2010
# Genome encoding:
#    For a gene presentation, I used a sequential representation where the cities are listed in the order in which they are visited. It's common way for TSP Genome.
#    Example: [A B C D E F G]
# Crossover:
#    I selected the Greedy Crossover by J. Grefenstette.
#    Greedy crossover selects the first city of one parent, compares the cities leaving that city in both parents, and chooses the closer one to extend the tour. If one city has already appeared in the tour, we choose the other city. If both cities have already appeared, we randomly select a non-selected city.
# Mutation: 
#    Example:
#    Before: [A B C D E F G]
#    After:  [A E D C B F G]
#    I didn't use any heuristic method in case of convergence into a local optimum.
# Selection: Roulette Wheel Rank Selection
#    Each generation contains 10 children.
#    Only one parent will be selected from each generation, while the other one will be the best throughout the evolution.
#    
# Haul condition: Complete 20000 generations.


# kod genetskog algoritma preuzet s http://pastebin.com/wgLaBbWR

# TODO potrebno preraditi i uklopiti u postojeci kod


import sys
import random
import copy
#import matplotlib.pyplot

src = sys.argv[1]
out = sys.argv[2]
outfile = open(out,"w")

class City:
    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y
    def __copy__(self):
        return City(self.name,self.x,self.y)
    def __eq__(self,other):
        return self.name == other.name and self.x == other.x and self.y == other.y

class World:
    cities = []
    l = None
    def __init__(self, route = None):
        if route != None:
            self.cities = route[:]
    def load(self, file):
        openfile = open(file,"r")
        lines = openfile.readlines()
        for i in xrange(1,int(lines[0])+1):
            line = lines[i].split()
            city = City(line[0],float(line[1]),float(line[2]))
            self.cities.append(city)
        openfile.close()
        
    
    def __copy__(self):
        return World(self.cities)
    def __lt__(self,other):
        return self.length() < other.length()
        
    def length(self):
        if self.l != None:
            return self.l
        self.l = 0.0
        for i in xrange(-1,len(self.cities)-1):
            cityA = self.cities[i]
            cityB = self.cities[i+1]
            self.l += ( (cityA.x-cityB.x)**2 + (cityA.y-cityB.y)**2 )**0.5
        return self.l
    
def distance(cityA, cityB):
    return ( (cityA.x-cityB.x)**2 + (cityA.y-cityB.y)**2 )**0.5

def crossover(mapA, mapB):
    flagA = True
    flagB = True
    length = len(mapA.cities)
    t = random.choice(mapA.cities)
    x = mapA.cities.index(t)
    y = mapB.cities.index(t)
    child = World([t])
    while flagA == True or flagB == True:
        x = (x - 1) % length
        y = (y + 1) % length
        if flagA == True:
            if mapA.cities[x] not in child.cities:
                child.cities.insert(0,mapA.cities[x])
            else:
                flagA = False
        if flagB == True:
            if mapB.cities[y] not in child.cities:
                child.cities.append(mapB.cities[y])
            else:
                flagB = False
    if len(child.cities) < length:
        rest = []
        for c in mapA.cities:
            if c not in child.cities:
                rest.append(c)
        random.shuffle(rest)
        child.cities += rest
    return child       
         
def mutate(route, p):
    if random.random() < p:
        i = j = 0
        mutated_route = copy.copy(route)
        while i == j:
            i = random.randint(0,len(route.cities)-1)
            j = random.randint(0,len(route.cities)-1)
        if i > j: i,j = j,i
        while i < j:
            mutated_route.cities[i],mutated_route.cities[j] = mutated_route.cities[j],mutated_route.cities[i]
            i+=1
            j-=1
        return mutated_route
    else:
        return route
            
def select(children):
    children.sort();
    p = []
    l = len(children)
    total = 0.0
    for i in xrange(1,l+1): total += i
    for i in xrange(l,0,-1): p.append(i/total)
    r = random.random();
    for rank, po in enumerate(p):
        if r < po:
            return children[rank]
        else:
            r -= po
   
def main():
    parentA = World()
    parentA.load(src)
    parentB = World(parentA.cities)
    random.shuffle(parentA.cities)

    t = 0
    best = parentA.length()
    while t < 20000:
        children = []
        #crossover and mutate
        for each in xrange(0,10):
            child = mutate(crossover(parentA, parentB), 0.3)
            children.append(child)
        parentA = select(children)      
        if parentA.length() < best:
            best = parentA.length()
            parentB = parentA
        t += 1
        if (t/20000.0)%0.1 < 0.00001:
            print (t/20000.0)*100,"% completed, the shortest length is",best
    print "100.0 % completed, the shortest length is",best
    print>>outfile, " ".join(c.name for c in parentB.cities),parentB.length()
    
if __name__ == "__main__":
    main()
