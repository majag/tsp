# glavni program koji poziva sve potrebne funkcije za rijesit problem TSP-a

#! usr/bin/env python3

import tsp_funkcije as tsp_f
import tsp_GA_funkcije as tsp_ga
import random
import copy
from mpi4py import MPI


broj_generacija = 20000
broj_djece = 15
postotak = 0.3


def main():
    out_file = "results_mpi.txt"
    #popis_gradova = "gradovi_brojevi.txt"
    popis_gradova = "gradovi_brojevi1.txt"
    #popis_gradova = "gradovi_brojevi2.txt"

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    assert size == 4

    if rank == 0:
        koordinate, gradovi = tsp_f.citaj_koordinate(popis_gradova)
        matrica = tsp_f.matrica_udaljenosti(koordinate, gradovi)
        posjeceni = tsp_f.inic_posjeceni(gradovi)
        tura, posjeceni = tsp_f.inic_tura(posjeceni, gradovi)

        posjeceni, tura, ukupna_cijena = tsp_f.tsp_algoritam_GA(matrica, posjeceni, tura, gradovi)
        print("Jesu li svi gradovi posjeceni: ", posjeceni)
        print("Tura kojom se posjete gradovi: ", tura)
        print("Ukupna cijena ture: ", ukupna_cijena)

        sendmsg = [tura, matrica, ukupna_cijena]
    else:
        sendmsg = None

    recvmsg = comm.bcast(sendmsg, root=0)
    tura = recvmsg[0]
    matrica = recvmsg[1]
    ukupna_cijena = recvmsg[2]


    turaA = copy.copy(tura)
    turaB = copy.copy(tura)
    random.shuffle(turaA)

    trenutna_generacija = 0
    najbolja_cijena = ukupna_cijena

    while trenutna_generacija < broj_generacija:
        ture_djeca = []
        #krizanje i mutacija
        for each in range(broj_djece):
            dijete = tsp_ga.mutacija(tsp_ga.krizanje(turaA, turaB), postotak)
            ture_djeca.append(dijete)
            
        #selekcija
        turaA = tsp_ga.selekcija(ture_djeca)
        nova_cijena = tsp_f.cijena_ture(turaA, matrica)
        
        if nova_cijena < najbolja_cijena:
            najbolja_cijena = nova_cijena
            turaB = copy.copy(turaA)
        trenutna_generacija += 1


    #recvmsg_result = comm.reduce(najbolja_cijena, op=MPI.MIN, root=0)
    recvmsg_result1 = comm.gather(turaB, root=0)
    recvmsg_result2 = comm.gather(najbolja_cijena, root=0)

    if rank == 0:
        min_cijena = recvmsg_result2[0]
        for elt in recvmsg_result2:
            if elt < min_cijena:
                min_cijena = elt
        t = recvmsg_result2.index(min_cijena)
        min_tura = recvmsg_result1[t]
        
        print("Tura nakon GA: ", min_tura)
        print("Cijena nakon GA: ", min_cijena)

        file = open(out_file, 'w')
        file.write('\nTura kojom se posjete gradovi: ' + str(tura))
        file.write('\nUkupna cijena ture: ' + str(ukupna_cijena))
        file.write('\nTura nakon GA: ' + str(min_tura))
        file.write('\nCijena nakon GA: ' + str(min_cijena))
        file.close()

if __name__ == "__main__":
    main()
